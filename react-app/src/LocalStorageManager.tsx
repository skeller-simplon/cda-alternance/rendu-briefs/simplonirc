import Channel from "./interfaces/Channel"

export class LocalStorageManager {
    protected static usernameKey: string = "USERNAME";
    protected static openChannelsKey: string = "OPEN_CHANNELS";

    static get Username(): string | null {
        return localStorage.getItem(this.usernameKey)
    }
    static set Username(username: string | null) {
        if (username !== null)
            localStorage.setItem(this.usernameKey, username)
        else
            localStorage.removeItem(this.usernameKey)
    }
    static get OpenChannels(): Channel[] {
        return JSON.parse(localStorage.getItem(this.openChannelsKey) || '[]') as Channel[]
    }
    static set OpenChannels(channels: Channel[]) {
        if (channels !== null)
            localStorage.setItem(this.openChannelsKey, JSON.stringify(channels))
        else
            localStorage.removeItem(this.openChannelsKey)
    }
    static setChannelAsNotified(channelId: number) {
        LocalStorageManager.OpenChannels = LocalStorageManager.OpenChannels.map(channel => {
            if (channel.Id === channelId)
                channel.Notified = true;
            return channel;
        })
        return LocalStorageManager.OpenChannels

    }
    static setChannelAsNotificationClosed(channelId: number) {
        console.log("close " + channelId);

        LocalStorageManager.OpenChannels = LocalStorageManager.OpenChannels.map(channel => {
            if (channel.Id === channelId)
                channel.Notified = false;
            return channel;
        })
        console.log(LocalStorageManager.OpenChannels);

        return LocalStorageManager.OpenChannels
    }
}