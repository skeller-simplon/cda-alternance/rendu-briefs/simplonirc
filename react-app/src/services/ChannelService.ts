import BaseService from "./BaseService"
import axios from "axios";
import Channel from "../interfaces/Channel";
export default class ChannelService extends BaseService {

    protected channelUrl: string = "";

    constructor() {
        super()
        this.channelUrl = this.apiUrl + "/channel/"
    }

    joinChannel(label: string, username: string) {
        return axios.post(
            this.channelUrl,
            { label: label }
        ).then(res => { return res })
    }

    getChannelByLabel(label: string): Promise<Channel> {
        return axios.get(
            this.channelUrl + label
        ).then(res => { return res.data });
    }

    getChannelsByUsername(username: string): Promise<Channel[]> {
        return axios.get(this.channelUrl + "username/" + username).then((res) => {
            return res.data.map((c: Channel) => new Channel(c.Label, c.Id))
        })
    }
}