import BaseService from "./BaseService"
import axios from "axios";
import Message from "../interfaces/Message"

export default class MessageService extends BaseService {
    protected messageUrl: string = "";

    constructor() {
        super()
        this.messageUrl = this.apiUrl + "/message/"
    }

    createMessage(message: string, username: string, channelId: number, creationDate: Date) {
        return axios.post(this.messageUrl, { message, username, channelId, creationDate }).then(res => {
            return res.data
        })
    }

    getMessagesByChannelId(channelId: number) {
        return axios.get(this.messageUrl + "channel/" + channelId).then(res => {
            return res.data.map((m: Message) => { return new Message(m.Id, m.Text, m.Username, m.CreationDate, m.ChannelId) })
        })
    }
}