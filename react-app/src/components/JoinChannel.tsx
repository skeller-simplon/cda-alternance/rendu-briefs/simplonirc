import React, { BaseSyntheticEvent, useState } from 'react';
import { LocalStorageManager } from '../LocalStorageManager';
import ChannelService from '../services/ChannelService';

function JoinChannel({ addChatRoom }: any) {
    let inputValue: string = "";
    let [channelService] = useState(new ChannelService());

    function updateInputValue(evt: BaseSyntheticEvent) {
        inputValue = evt.target.value;
    }

    function submitForm() {
        if (!LocalStorageManager.Username)
            throw new Error("Non connecté")
        channelService.joinChannel(inputValue, LocalStorageManager.Username)
        addChatRoom(inputValue)
    }
    return (
        <div id="join-channel">
            <form>
                <input type="text" id="channelName" placeholder="Nom du salon" onChange={updateInputValue} />
                <input type="button" name="submit" value="OK" onClick={(e) => submitForm()} />
            </form>
        </div>
    );
}

export default JoinChannel;
