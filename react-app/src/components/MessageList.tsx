import { useEffect, useState } from "react";
import MessageService from "../services/MessageService";
import Message from "../interfaces/Message"
import { FaUser } from 'react-icons/fa';
import SocketManager from "../SocketManagerFront";
import { LocalStorageManager } from "../LocalStorageManager";

interface MessageListProps {
    channelId: number
}
function MessageList({ channelId }: MessageListProps) {
    let [messageService] = useState(new MessageService());
    let [messages, setMessages] = useState([] as Message[])

    useEffect(() => {
        refreshMessages(messageService, channelId, setMessages)
        LocalStorageManager.setChannelAsNotificationClosed(channelId)

    }, [channelId, messageService]);

    SocketManager.subscribeToRefresher(channelId, (msgs: Message[]) => setMessages(msgs || []))

    function refreshMessages(messageService: MessageService, channelId: number, setMessages: Function) {
        messageService.getMessagesByChannelId(channelId).then(msgs => {
            console.log("messages");

            setMessages(msgs);
        });
    }
    return (
        <div id="message-list">
            {messages.map((message) => {
                return (<div className="message bubble" key={message.Id}>
                    <small><FaUser /> {message.Username} - {message.DisplayCreationDate}</small>
                    <h4>{message.Text}</h4>
                </div >)
            })}
        </div>
    );
}

export default MessageList;


