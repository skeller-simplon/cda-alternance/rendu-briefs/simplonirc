import { BaseSyntheticEvent, useState } from "react";
import { LocalStorageManager } from "../LocalStorageManager";
import MessageService from "../services/MessageService"

interface MessageFormProps {
    channelId: number
}

function MessageForm({ channelId }: MessageFormProps) {
    let inputMessage: string = "";
    let [messageService] = useState(new MessageService());

    function updateInputValue(evt: BaseSyntheticEvent) {
        inputMessage = evt.target.value;
    }

    function submitForm() {
        if (!LocalStorageManager.Username)
            throw new Error("Non connecté")
        if (inputMessage.trim() === "")
            throw new Error("Pas de message")
        messageService.createMessage(inputMessage, LocalStorageManager.Username, channelId, new Date()).then(() => {
            inputMessage = "";
        })
    }
    return (
        <div id="message-form">
            <form>
                <input type="text" id="channelName" placeholder="Message" onChange={updateInputValue} />
                <input type="button" name="submit" value="Envoyer" onClick={(e) => submitForm()} />
            </form>
        </div>
    );
}

export default MessageForm;
