import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { FaPowerOff, FaPlus, FaExclamationCircle } from 'react-icons/fa';
import { useState } from 'react';
import JoinChannel from "../components/JoinChannel"
import ChannelService from '../services/ChannelService';
import MessageList from "../components/MessageList"
import MessageForm from "../components/MessageForm"
import { LocalStorageManager } from '../LocalStorageManager';

function ChatPage({ logOut }: any) {
    const [openChannels, setOpenChats] = useState(LocalStorageManager.OpenChannels);
    const [displayCreateChannel, setDisplayCreateChannel] = useState(false);
    const [tabIndex, setTabIndex] = useState(0);
    const [channelService] = useState(new ChannelService());

    function addChatRoom(label: string) {
        if (openChannels.map(c => c.Label).includes(label)) {
            return;
        }
        setDisplayCreateChannel(false)

        channelService.getChannelByLabel(label).then(channel => {
            let updatedChannelsList = [...openChannels, channel]
            setOpenChats(updatedChannelsList);
            LocalStorageManager.OpenChannels = updatedChannelsList;

            // Workaround : l'index défini par react-tabs est la position dans l'array,
            // donc après un push on peut utilise la longueur.
            setTabIndex(openChannels.length)
        })

    }
    return (
        <div id="chatPage">

            <Tabs selectedIndex={tabIndex} onSelect={index => setTabIndex(index)}>
                <TabList>
                    {openChannels.map((channel) => {
                        return <Tab key={channel.Id}>
                            {channel.Notified ? <FaExclamationCircle /> : <></>}
                            <h4>{channel.Label}</h4></Tab>
                    })}
                    <section id="chatBar">
                        {displayCreateChannel ?
                            <JoinChannel addChatRoom={addChatRoom} /> :
                            <button onClick={() => setDisplayCreateChannel(true)}> <FaPlus /> Rejoindre un salon</button>
                        }

                        <button onClick={logOut}> <FaPowerOff /> Deconnexion</button>
                    </section>
                </TabList>

                {openChannels.map((channel) => {
                    return <TabPanel key={channel.Id}>
                        <MessageList channelId={channel.Id} />
                        <MessageForm channelId={channel.Id} />
                    </TabPanel>
                })}
            </Tabs>
        </div>
    );
}

export default ChatPage;
