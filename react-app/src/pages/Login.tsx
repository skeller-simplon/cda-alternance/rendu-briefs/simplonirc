import React, { BaseSyntheticEvent, useState } from 'react';
import { LocalStorageManager } from '../LocalStorageManager';
import ChannelService from '../services/ChannelService';

function Login({ refresh }: any) {
    let inputValue: string = "";
    let [channelService] = useState(new ChannelService());
    function updateInputValue(evt: BaseSyntheticEvent) {
        inputValue = evt.target.value;
    }

    function submitForm() {
        if (inputValue) {
            LocalStorageManager.Username = inputValue;
            channelService.getChannelsByUsername(inputValue).then(channels => {
                LocalStorageManager.OpenChannels = channels;
                refresh()
            })
        }
    }
    return (
        <div id="login">
            <form>
                <label htmlFor="login">Username</label>
                <input type="text" name="login" id="login" placeholder="Username" onChange={updateInputValue} />
                <input type="button" name="submit" value="OK" onClick={(e) => submitForm()} />
            </form>
        </div>
    );
}

export default Login;
