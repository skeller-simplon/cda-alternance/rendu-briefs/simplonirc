import openSocket from "socket.io-client"
import Message from "./interfaces/Message";
import { LocalStorageManager } from "./LocalStorageManager";


const REFRESH_LIST_KEY = "REFRESH_LIST_KEY";

class SocketManagerFront {
    static socket: SocketIOClient.Socket;

    static start() {
        this.socket = openSocket('http://localhost:8000');
    }
    static subscribeToRefresher(channelId: number, messagesSetter: Function) {
        this.socket.on(REFRESH_LIST_KEY, (messages: Message[]) => {
            console.log("Socket - Getting new list of messages for conversation " + channelId);
            messagesSetter(messages[channelId])
            LocalStorageManager.setChannelAsNotified(channelId)
        });
    }
}

export default SocketManagerFront