import React, { useState } from 'react';
import { LocalStorageManager } from './LocalStorageManager';
import ChatPage from './pages/ChatPage';
import Login from './pages/Login';

function App() {

  const [username, setUsername] = useState(LocalStorageManager.Username);

  function refetchUsername() {
    setUsername(LocalStorageManager.Username)
  }

  function logOut() {
    LocalStorageManager.Username = null;
    LocalStorageManager.OpenChannels = [];
    refetchUsername();
  }

  return (
    <div className="App">
      { username ?
        (<ChatPage logOut={logOut} />) :
        (<Login refresh={refetchUsername} />)
      }
    </div>
  );
}

export default App;
