export default class Channel {
    public Label: string;
    public Id: number;
    protected _Notified: boolean = false;

    constructor(label: string, id: number) {
        this.Label = label;
        this.Id = id;
    }

    get Notified(): boolean {
        return this._Notified
    }
    set Notified(notified: boolean) {
        this._Notified = notified;
    }

}