import moment from "moment"
import "moment/locale/fr"

moment.defineLocale("fr", {})

export default class Message {
    public Id: number;
    public Text: string;
    public Username: string;
    public CreationDate: Date;
    public ChannelId: number;
    constructor(id: number, text: string, username: string, creationDate: Date, channelId: number) {
        this.Id = id;
        this.Text = text;
        this.Username = username;
        this.CreationDate = creationDate;
        this.ChannelId = channelId
    }

    public get DisplayCreationDate(): string {
        return moment(this.CreationDate).locale("fr").calendar()
    }
}