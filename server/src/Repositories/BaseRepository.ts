import { Connection, queryCallback } from "mysql";
import dbConnection from "../db/db"

// Classe générique de requêtes base de donnée.
// Doit être héritée en redifinissant tableName.
export default class BaseRepository<T> {
    public connection: Connection = dbConnection;
    public tableName: string = null;

    protected getOneById = (id: number): Promise<T> => {
        this.throwIfNoTableName()
        return new Promise((res, rej) => {
            this.connection.query(`SELECT * FROM ${this.tableName} where id = ? LIMIT 1`,
                [id], (err, [servRes]) => { res(servRes) })
        })
    }

    protected getAll = (): Promise<T[]> => {
        this.throwIfNoTableName();
        return new Promise((res, rej) => {
            this.connection.query(`SELECT * FROM ${this.tableName}`,
                (err, servRes) => { res(servRes) })
        })
    }

    protected throwIfNoTableName = () => {
        if (!this.tableName) throw Error("Mauvais nom de table")
    }
}