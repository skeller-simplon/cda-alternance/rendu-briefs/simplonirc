import BaseRepository from "./BaseRepository"
import Message from "../lib/Message"

export default class MessageRepository extends BaseRepository<Message> {
    constructor() {
        super();
        this.tableName = "Message";
    }

    create(message: string, username: string, channelId: number, creationDate: Date): Promise<any> {
        return new Promise((res, rej) => {
            this.connection.query(`INSERT INTO ${this.tableName} (Text, Username, ChannelId, CreationDate) VALUES (?, ?, ?, ?)`,
                [message, username, channelId, creationDate], (err, servRes) => {
                    if (err)
                        rej(err)
                    res(servRes)
                })
        })
    }

    getAllByChannelId(channelId: string) {
        return new Promise((res, rej) => {
            this.connection.query(`SELECT * FROM ${this.tableName} WHERE ChannelId = ?;`,
                [channelId], (err, servRes) => {
                    if (err)
                        rej(err)
                    res(servRes)
                })
        })


    }
}