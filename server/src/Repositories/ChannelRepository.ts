import BaseRepository from "./BaseRepository"
import Channel from "../lib/Channel"

export default class ChannelRepository extends BaseRepository<Channel> {
    constructor() {
        super();
        this.tableName = "Channel";
    }

    async create(label: string): Promise<any> {
        return new Promise((res, rej) => {
            this.connection.query(`INSERT INTO ${this.tableName} (Label) VALUES (?)`,
                [label], (err, servRes) => {
                    if (err)
                        rej(err)
                    res(servRes)
                })
        })
    }

    getByLabel(label: any): Promise<Channel> {
        return new Promise((res, rej) => {
            this.connection.query(`SELECT * FROM ${this.tableName} WHERE Label = ? LIMIT 1;`,
                [label], (err, [servRes]) => {
                    if (err)
                        rej(err)
                    res(servRes)
                })
        })
    }

    getChannelsIdsByUsername(username: string): Promise<number[]> {
        return new Promise((res, rej) => {
            this.connection.query(`SELECT ChannelId FROM Message WHERE Username = ?`,
                [username], (err, servRes) => {
                    if (err)
                        rej(err)
                    res(servRes.map(i => { return i.ChannelId }))
                })
        })
    }

    getChannelsByIds(ids: number[]): Promise<Channel[]> {
        return new Promise((res, rej) => {
            this.connection.query(`SELECT * FROM simplonirc.Channel where Id in (?);`,
                [ids.join(", ")], (err, servRes) => {
                    if (err)
                        rej(err)
                    res(servRes || [])
                })
        })
    }
}