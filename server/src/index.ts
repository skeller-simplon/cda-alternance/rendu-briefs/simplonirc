import express from "express";
import routes from "./routes/routes";
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import cors from 'cors';
import SocketManager from "./SocketManager";

dotenv.config();

const app = express();
const port = 8080 || process.env.DB_port;

// Bodyparser - traite les requêtes.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use(cors({ origin: "*", methods: "*" }))

app.listen(port, () => {
    console.log(`Serveur sur port ${port}`);
});

app.use("/api", routes);

export default app;