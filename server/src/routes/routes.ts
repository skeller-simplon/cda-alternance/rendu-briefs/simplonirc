import express from 'express'
const router = express()

import ChannelController from "./ChannelController";
import MessageController from './MessageController';

router.use("/channel", ChannelController);
router.use("/message", MessageController);

export default router;
