import express from "express";
import Channel from "../lib/Channel";
import ChannelService from "../services/ChannelService";

const ChannelController = express.Router();
const _channelService = new ChannelService();

ChannelController.post("/", (req, res) => {
    try {
        if (!req.body.label)
            throw new Error("Libellé manquant")
        _channelService.createChannel(req.body.label).then(
            (channel: Channel) => {
                res.send(channel);
            }).catch(err => { res.status(401).send(err) })
    } catch (error) {
        res.status(401).send(error)
    }
})

ChannelController.get("/:label", (req, res) => {
    try {
        if (!req.params.label)
            throw new Error("Libellé manquant")

        _channelService.getByLabel(req.params.label).then((channel: Channel) => {
            res.send(channel);
        }).catch(err => { res.status(401).send(err) })
    } catch (error) {
        res.status(401).send(error)
    }
})

ChannelController.get("/username/:username", (req, res) => {
    try {
        if (!req.params.username)
            throw new Error("Nom d'utilisateur manquant")

        _channelService.getChannelsByUsername(req.params.username).then((channels: Channel[]) => {
            res.send(channels);
        }).catch(err => {
            res.status(401).send(err)
        })
    } catch (error) {
        res.status(401).send(error)
    }
})

export default ChannelController