import express from "express";
import Channel from "../lib/Channel";
import MessageService from "../services/MessageService";

const MessageController = express.Router();
const _channelService = new MessageService();

MessageController.post("/", (req, res) => {
    try {
        if (!req.body.message)
            throw new Error("Message manquant")
        if (!req.body.username)
            throw new Error("Pseudo manquant")
        if (!req.body.channelId)
            throw new Error("Identifiant de channel manquant")
        if (!req.body.creationDate)
            req.body.creationDate = new Date;
        else
            req.body.creationDate = new Date(req.body.creationDate)

        _channelService.createMessage(req.body.message, req.body.username, parseInt(req.body.channelId), req.body.creationDate).then(r => {
            res.send(r)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

MessageController.get("/channel/:channelId", (req, res) => {
    try {
        if (!req.params.channelId)
            throw new Error("Identifiant de channel manquant")

        _channelService.getMessagesByChannelId(req.params.channelId).then(r => {
            res.send(r)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

export default MessageController