import Channel from "../lib/Channel";
import ChannelRepository from "../Repositories/ChannelRepository";

export default class ChannelService {
    private repository: ChannelRepository = new ChannelRepository();

    createChannel(label: any): Promise<any> {
        return this.repository.getByLabel(label).then(channel => {
            // S'il existe déjà, on passe
            if (channel)
                return channel
            else
                return this.repository.create(label)
        })
    }

    public getByLabel(label: any): Promise<Channel> {
        return this.repository.getByLabel(label)
    }

    getChannelsIdsByUsername(username: string): Promise<number[]> {
        return this.repository.getChannelsIdsByUsername(username).then(ids => {
            return ids ? ids : []
        })
    }
    getChannelsByUsername(username: string): Promise<Channel[]> {
        return this.getChannelsIdsByUsername(username).then((ids: number[]) => {
            return this.repository.getChannelsByIds(ids).then(t => {
                return t
            })
        })
    }
}