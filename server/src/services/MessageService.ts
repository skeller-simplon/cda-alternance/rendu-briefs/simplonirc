import socket from "../SocketManager";
import Message from "../lib/Message";
import MessageRepository from "../Repositories/MessageRepository";

export default class MessageService {
    private repository: MessageRepository = new MessageRepository();

    createMessage(message: string, username: string, channelId: number, creationDate: Date): Promise<any> {
        let SQLResult: Promise<any> = this.repository.create(message, username, channelId, creationDate)
        socket.refreshMessageList(channelId)
        return SQLResult
    }

    getMessagesByChannelId(channelId: string) {
        return this.repository.getAllByChannelId(channelId)
    }
}