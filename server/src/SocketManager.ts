import { Socket } from "net";
import * as io from "socket.io";
import app from "./index";
import MessageService from "./services/MessageService";

var http = require('http')

const REFRESH_LIST_KEY = "REFRESH_LIST_KEY";

export class SocketManager {
    protected socketServer: io.Server;
    protected socketPort = 8000;
    protected httpServ;

    constructor(app) {
        this.httpServ = http.Server(app)
        this.socketServer = new io.Server({
            cors: {
                origin: '*',
                methods: '*'
            }
        });

    }
    connect() {
        this.socketServer.on("connection", (socket) => {
            console.log("Socket - Connection opened")
        })
        this.socketServer.listen(this.socketPort)
    }

    refreshMessageList = (channelId: number) => {
        new MessageService().getMessagesByChannelId(channelId.toString()).then(messages => {
            let messagesToUpdate = [];
            messagesToUpdate[channelId] = messages
            this.socketServer.emit(REFRESH_LIST_KEY, messagesToUpdate)

        })
    }
}



let socket = new SocketManager(app)
socket.connect();

export default socket