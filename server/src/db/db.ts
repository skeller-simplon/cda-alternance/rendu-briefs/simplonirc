import dotenv from "dotenv";
dotenv.config();
import mysql from "mysql";

const dbConnection = mysql.createConnection({
    host: "localhost",
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.DB_NAME,
});

dbConnection.connect((err: Error) => {
    if (err) throw err;

    // Création de la table Channel si elle n'existe pas
    checkTableOfCreateIt(
        "Channel",
        `CREATE TABLE Channel (
      Id INT NOT NULL AUTO_INCREMENT,
      Label VARCHAR(45) NOT NULL UNIQUE,
      PRIMARY KEY (Id),
      UNIQUE INDEX Id_UNIQUE (Id ASC));
    `
    );

    // Création de la table Message si elle n'existe pas
    checkTableOfCreateIt(
        "Message",
        `CREATE TABLE Message (
      Id INT UNSIGNED NOT NULL AUTO_INCREMENT,
      Text VARCHAR(45) NOT NULL,
      CreationDate DATETIME NOT NULL,
      Username VARCHAR(45) NOT NULL,
      ChannelId INT NOT NULL,
      PRIMARY KEY (Id),
      CONSTRAINT fk_Table_Message
        FOREIGN KEY (ChannelId)
        REFERENCES Channel (Id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)`
    );
});

function checkTableOfCreateIt(tableName: string, query: string) {
    dbConnection.query(
        "SHOW TABLES LIKE '" + tableName + "'",
        (error, results) => {
            if (error) return console.log(error);
            if (!results.length) {
                console.log(`Création de la table ${tableName}`);
                dbConnection.query(query);
            }
        }
    );
}

export default dbConnection;
