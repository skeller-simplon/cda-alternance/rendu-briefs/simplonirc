export default class Channel {
    public Label: string;
    public Text: string;
    public Username: string;
    public CreationDate: Date;
    public ChannelId: number;
}